# k8s-app-template
Bootstrap a new k8s application template

Usage: `ansible-playbook -c local setup.yml -e app_name=app -e author='Antoine Legrand' -e license='MIT'`
