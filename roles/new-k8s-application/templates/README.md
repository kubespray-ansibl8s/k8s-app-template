k8s-{{app_name}}
==============

{% if description is defined %}
{{description}}
{% endif %}

Requirements
--------------

 - kubernetes

Role Variables
----------------

Default variables:

```yaml
{{ lookup('file', dest + '/defaults/main.yml')}}
```

Dependencies
------------
 - k8s-common

Example Playbook
----------------
Ferm rules are hash instead of array. The main reason is to be able to merge hashes when configure same host with different roles.
```yaml
- hosts: kube-master
  vars:
    author: ant31
    company: myCompany
    app_name: myapp
    license: MIT
    dest: /tmp/k8s-myapp
    description: K8s role to deploy myapp....
  roles:
    - k8s-myapp
```

{% if license is defined %}
License
-------
{{license}}
{% endif %}
